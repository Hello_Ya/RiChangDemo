package com.example.ljw.richangdemo.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ljw.richangdemo.R;
import com.example.ljw.richangdemo.base.LazyLoadBaseFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ljw on 2018/5/10.
 * function：
 */

public class LazyFragment4 extends LazyLoadBaseFragment {

    @Bind(R.id.tv_text)
    TextView tvText;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private String text = getClass().getSimpleName() + " ";


    public static LazyFragment4 newInstance(String params) {
        LazyFragment4 fragment = new LazyFragment4();
        Bundle args = new Bundle();
        args.putString("params", params);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragmnet_text;
    }

    @Override
    protected void initView(View rootView) {
        tvText = (TextView) rootView.findViewById(R.id.tv_text);
        tvText.setText(text);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
