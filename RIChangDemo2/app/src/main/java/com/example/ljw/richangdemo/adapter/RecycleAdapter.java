package com.example.ljw.richangdemo.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.ljw.richangdemo.R;
import com.example.ljw.richangdemo.bean.DemoBean;

import java.util.List;

/**
 * Created by ljw on 2017/12/5.
 * function：
 */

public class RecycleAdapter extends BaseQuickAdapter<DemoBean.ListBean, BaseViewHolder> {

    private Context mContent;

    public RecycleAdapter(Context content, @LayoutRes int layoutResId, @Nullable List<DemoBean.ListBean> data) {
        super(layoutResId, data);
        this.mContent = content;
    }



    @Override
    protected void convert(BaseViewHolder helper, final DemoBean.ListBean item) {
        final CheckBox mCheckBox = helper.getView(R.id.cb_checked);
        mCheckBox.setChecked(item.isCheck());

            helper.addOnClickListener(R.id.cb_checked);

        mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                item.setCheck(isChecked);


            }
        });

        helper.setText(R.id.tv_text, item.getmTitle());

    }


}
