package com.example.ljw.richangdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.ljw.richangdemo.activity.BannerActivity;
import com.example.ljw.richangdemo.activity.LazyTestActivity;
import com.example.ljw.richangdemo.activity.TablayoutActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.btn_recycle)
    Button btnRecycle;
    @Bind(R.id.btn_tablayout)
    Button btnTablayout;
    @Bind(R.id.btn_TablayoutActivity)
    Button btnTablayoutActivity;
    @Bind(R.id.btn_BannerActivity)
    Button btnBannerActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }


    @OnClick({R.id.btn_recycle, R.id.btn_tablayout, R.id.btn_BannerActivity,R.id.btn_TablayoutActivity})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_recycle:
                startActivity(new Intent(this, RecycleviewActivity.class));


                break;
            case R.id.btn_tablayout:
                startActivity(new Intent(this, TablayoutActivity.class));

                break;
            case R.id.btn_BannerActivity:

                startActivity(new Intent(this, BannerActivity.class));

                break;

            case R.id.btn_TablayoutActivity: //懒加载

                startActivity(new Intent(this, LazyTestActivity.class));

                break;
        }
    }
}
