package com.example.ljw.richangdemo.bean;

/**
 * Created by lijiafu on 2017/12/6.
 * 功能：
 */

public class DemoBean {

    private ListBean mLiset;

    public DemoBean(ListBean mLiset) {
        this.mLiset = mLiset;
    }

    public static class ListBean {

        private String mTitle;
        private boolean isCheck;

        public ListBean(String mTitle, boolean isCheck) {
            this.mTitle = mTitle;
            this.isCheck = isCheck;
        }

        public String getmTitle() {
            return mTitle;
        }

        public void setmTitle(String mTitle) {
            this.mTitle = mTitle;
        }

        public boolean isCheck() {
            return isCheck;
        }

        public void setCheck(boolean check) {
            isCheck = check;
        }
    }
}
