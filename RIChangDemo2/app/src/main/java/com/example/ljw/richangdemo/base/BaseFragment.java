package com.example.ljw.richangdemo.base;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ljw.richangdemo.MyApplication;
import com.example.ljw.richangdemo.utils.ToastUtil;
import com.google.gson.Gson;

import butterknife.ButterKnife;


/**
 * Fragment 基类
 */
public abstract class BaseFragment extends Fragment {

    public String TAG = getClass().getSimpleName();
    public MyApplication app;
  //  public HttpUtils httpUtils;
    public Gson gson;
    private FragmentActivity content;
    public Context context;
    private ProgressDialog progressDialog;
    public BaseFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        super.onCreateView(inflater, container,  savedInstanceState);
        context = getContext();
        app = (MyApplication) getContext().getApplicationContext();


     //   httpUtils = app.httpUtils;
     //   gson = app.gson;
        View view = inflater.inflate(setContentView(), container, false);
        ButterKnife.bind(this, view);
        initView(view);
        return view;
    }


    /**
     * 设置Fragment要显示的布局
     *
     * @return 布局的layoutId
     */
    protected abstract int setContentView();

    /**
     * 初始化控件操作
     *
     */
    protected abstract void initView(View view);

    @Override
    public void onResume() {
        super.onResume();
    //    int goodsCount = ShoppingCartBiz.getGoodsCount();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
         ButterKnife.unbind(this);
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void toast(String msg) {
        ToastUtil.shortToast(MyApplication.getContext(), msg);
    }

    public void toast(int resId) {
        ToastUtil.shortToast(MyApplication.getContext(), resId);
    }



    public void showProgressDialog() {
        if (progressDialog == null) {

            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("正在加载数据...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }

        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

}
