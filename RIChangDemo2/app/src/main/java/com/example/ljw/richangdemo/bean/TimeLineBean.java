package com.example.ljw.richangdemo.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ljw on 2017/12/7.
 * function：
 */

public class TimeLineBean implements Parcelable{

    private String mMessage;
    private String mDate;
    private boolean mStatus;

    public TimeLineBean() {
    }

    public TimeLineBean(String mMessage, String mDate, boolean mStatus) {
        this.mMessage = mMessage;
        this.mDate = mDate;
        this.mStatus = mStatus;
    }


    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }

    public boolean ismStatus() {
        return mStatus;
    }

    public void setmStatus(boolean mStatus) {
        this.mStatus = mStatus;
    }

    protected TimeLineBean(Parcel in) {
        mMessage = in.readString();
        mDate = in.readString();
        mStatus = in.readByte() != 0;
    }

    public static final Creator<TimeLineBean> CREATOR = new Creator<TimeLineBean>() {
        @Override
        public TimeLineBean createFromParcel(Parcel in) {
            return new TimeLineBean(in);
        }

        @Override
        public TimeLineBean[] newArray(int size) {
            return new TimeLineBean[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mMessage);
        parcel.writeString(mDate);
        parcel.writeByte((byte) (mStatus ? 1 : 0));
    }
}
