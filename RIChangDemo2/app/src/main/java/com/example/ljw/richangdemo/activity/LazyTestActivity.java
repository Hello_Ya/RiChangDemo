package com.example.ljw.richangdemo.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import com.example.ljw.richangdemo.R;
import com.example.ljw.richangdemo.adapter.LazyViewPagerAdapter;
import com.example.ljw.richangdemo.base.BaseActivity;
import com.example.ljw.richangdemo.fragment.LazyFragment1;
import com.example.ljw.richangdemo.fragment.LazyFragment2;
import com.example.ljw.richangdemo.fragment.LazyFragment3;
import com.example.ljw.richangdemo.fragment.LazyFragment4;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ljw on 2018/5/10.
 * function：
 */

public class LazyTestActivity extends BaseActivity implements ViewPager.OnPageChangeListener {
    @Bind(R.id.viewPager)
    ViewPager viewPager;
    @Bind(R.id.design_bottom_sheet)
    BottomNavigationView designBottomSheet;



    private LazyFragment1 fragment1 = LazyFragment1.newInstance("渣渣辉");
    private LazyFragment2 fragment2 = LazyFragment2.newInstance("古天乐");
    private LazyFragment3 fragment3 = LazyFragment3.newInstance("游戏");
    private LazyFragment4 fragment4 = LazyFragment4.newInstance("贪玩蓝月");
    ArrayList<Fragment> fragments = new ArrayList<>(4);

    @Override
    protected int setContentViewID() {
        return R.layout.activity_lazyfragmenttext;
    }

    @Override
    protected void initView() {


    }

    @Override
    protected void initData() {
        fragments.add(fragment1);
        fragments.add(fragment2);
        fragments.add(fragment3);
        fragments.add(fragment4);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        //添加viewPager事件监听（很容易忘）
        viewPager.addOnPageChangeListener(this);
        designBottomSheet = (BottomNavigationView) findViewById(R.id.design_bottom_sheet);
        designBottomSheet.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        viewPager.setAdapter(new LazyViewPagerAdapter(getSupportFragmentManager(), fragments, null));

    }

    @Override
    protected void initListener() {

    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            //点击BottomNavigationView的Item项，切换ViewPager页面
            //menu/navigation.xml里加的android:orderInCategory属性就是下面item.getOrder()取的值
            viewPager.setCurrentItem(item.getOrder());
            return true;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }



    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        //页面滑动的时候，改变BottomNavigationView的Item高亮
        designBottomSheet.getMenu().getItem(position).setChecked(true);

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
