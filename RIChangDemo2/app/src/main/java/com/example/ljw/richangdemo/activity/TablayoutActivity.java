package com.example.ljw.richangdemo.activity;

import android.support.v4.view.ViewPager;

import com.example.ljw.richangdemo.R;
import com.example.ljw.richangdemo.adapter.ViewPagerAdapter;
import com.example.ljw.richangdemo.base.BaseActivity;
import com.example.ljw.richangdemo.bean.PagerInfo;
import com.example.ljw.richangdemo.fragment.OtherFragment;
import com.example.ljw.richangdemo.fragment.TimeLineFragment;
import com.flyco.tablayout.SlidingTabLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by ljw on 2017/12/7.
 * function：
 */

public class TablayoutActivity extends BaseActivity {

    @Bind(R.id.slitab_1)
    SlidingTabLayout slitab1;
    @Bind(R.id.vp_type)
    ViewPager vpType;

    ViewPagerAdapter mAdapter;
    private String[] title = {"正在热评", "完成热评"};

    @Override
    protected int setContentViewID() {
        return R.layout.activity_tablayout;
    }

    @Override
    protected void initView() {
        List<PagerInfo> pagerList = new ArrayList<PagerInfo>();
        pagerList.add(new PagerInfo(new OtherFragment(), title[0]));
        pagerList.add(new PagerInfo(new TimeLineFragment(), title[1]));

        mAdapter = new ViewPagerAdapter(getSupportFragmentManager(), pagerList);
        vpType.setAdapter(mAdapter);
        slitab1.setViewPager(vpType);
        vpType.setCurrentItem(0);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }
}
