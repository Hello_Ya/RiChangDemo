package com.example.ljw.richangdemo;

import android.app.Application;
import android.content.Context;

import com.zhy.autolayout.config.AutoLayoutConifg;

/**
 * Created by ljw on 2017/12/5.
 * function：
 */

public class MyApplication extends Application {

    public static Context mContext;
    @Override
    public void onCreate() {
        super.onCreate();

        AutoLayoutConifg.getInstance().useDeviceSize();
        mContext = getApplicationContext();
    }

    public static Context getContext() {
        return mContext;
    }

}
