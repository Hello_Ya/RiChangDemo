package com.example.ljw.richangdemo.bean;

import android.support.v4.app.Fragment;

import java.io.Serializable;

/**
 * Created by Ocean on 6/22/16.
 */
public class PagerInfo implements Serializable {
    private Fragment fragment;
    private int titleResId;
    private String titleRes;

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public int getTitleResId() {
        return titleResId;
    }

    public String getTitleRes() {
        return titleRes;
    }


    public void setTitleResId(int titleResId) {
        this.titleResId = titleResId;
    }

    public PagerInfo(Fragment fragment, int titleResId) {
        this.fragment = fragment;
        this.titleResId = titleResId;
    }

    public PagerInfo(Fragment fragment, String titleResId) {
        this.fragment = fragment;
        this.titleRes = titleResId;
    }
}
