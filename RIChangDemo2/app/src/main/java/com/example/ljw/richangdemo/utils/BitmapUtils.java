package com.example.ljw.richangdemo.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.WindowManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Atom on 2016/7/16.
 */
public class BitmapUtils {

    private int screenWidth;
    private int screenHeight;

    public BitmapUtils(Context context) {
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        screenWidth = manager.getDefaultDisplay().getWidth();
        screenHeight = manager.getDefaultDisplay().getHeight();

    }

    /**
     * 获取一个按屏幕比例压缩过后的bitmap
     *
     * @param imageFilePath 图片路径
     * @return bitmap
     */
    public Bitmap getCompressBitmap(String imageFilePath) {

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;

        // 去获得图片的附属信息，比如宽高
        BitmapFactory.decodeFile(imageFilePath, opts);

        // 为了计算压缩比例，最好用手机屏幕的宽高去除，得到一个比例值

        int width = opts.outWidth;
        int height = opts.outHeight;

        int widthScale = width / (screenWidth / 3);
        int heiScale = height / (screenHeight / 3);

        // 取比例大的那个数
        int scale = widthScale > heiScale ? widthScale : heiScale;

        opts.inSampleSize = scale;  //
        Log.e("inSampleSize::", scale + "");
        // 将这个开关关闭，真正的加载到内存中
        opts.inJustDecodeBounds = false;
        opts.inPreferredConfig = Bitmap.Config.RGB_565;

        // 这次才拿到图片，但是比例是原先的 widthScale这个倍数
        return BitmapFactory.decodeFile(imageFilePath, opts);
    }

    public Bitmap getBitmap(String imageFilePath) {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;

        // 去获得图片的附属信息，比如宽高
        BitmapFactory.decodeFile(imageFilePath, opts);

        // 为了计算压缩比例，最好用手机屏幕的宽高去除，得到一个比例值

        int width = opts.outWidth;
        int height = opts.outHeight;

        int widthScale = width / screenWidth;
        int heiScale = height / screenHeight;

        // 取比例大的那个数
        int scale = widthScale > heiScale ? widthScale : heiScale;

        opts.inSampleSize = scale;  //
        Log.e("inSampleSize1::", scale + "");
        // 将这个开关关闭，真正的加载到内存中
        opts.inJustDecodeBounds = false;
        opts.inPreferredConfig = Bitmap.Config.RGB_565;

        // 这次才拿到图片，但是比例是原先的 widthScale这个倍数
        return BitmapFactory.decodeFile(imageFilePath, opts);
    }

    /**
     * @param bm
     * @return
     */
    public static Uri saveBitmap(String fileName, Bitmap bm) {
        File tmpDir = new File(Environment.getExternalStorageDirectory() + "/pinche");
        if (!tmpDir.exists()) {
            tmpDir.mkdir();
        }
        File img = new File(tmpDir.getAbsolutePath() + "/" + fileName + System.currentTimeMillis() + ".png");

        try {
            FileOutputStream fos = new FileOutputStream(img);
            bm.compress(Bitmap.CompressFormat.PNG, 85, fos);
            fos.flush();
            fos.close();
            return Uri.fromFile(img);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * bitmap转为base64
     *
     * @param bitmap
     * @return base64 编码
     */
    public static String bitmapToBase64(Bitmap bitmap) {

        String result = null;
        ByteArrayOutputStream baos = null;
        try {
            if (bitmap != null) {
                baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                baos.flush();
                baos.close();

                byte[] bitmapBytes = baos.toByteArray();
                result = Base64.encodeToString(bitmapBytes, Base64.DEFAULT);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (baos != null) {
                    baos.flush();
                    baos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * base64转为bitmap
     *
     * @param base64Data
     * @return
     */
    public static Bitmap base64ToBitmap(String base64Data) {
        byte[] bytes = Base64.decode(base64Data, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    /**
     * 把Bitmap转Byte
     */
    public static byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }
}
