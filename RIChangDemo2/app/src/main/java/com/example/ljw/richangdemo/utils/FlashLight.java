package com.example.ljw.richangdemo.utils;

import android.hardware.Camera;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

/**
 * <!-- 手电筒 -->
 * Call requires API level 5
 * <uses-permission android:name="android.permission.FLASHLIGHT"/>
 * <uses-permission android:name="android.permission.CAMERA"/>
 *
 * @author MaTianyu
 * @date 2014-11-04
 */
public class FlashLight {

    private Camera camera;
    private static Handler handler = new Handler();

    /**
     * 超过2分钟自动关闭，防止损伤硬件
     */
    private static final int OFF_TIME = 2 * 60 * 1000;

    public boolean turnOnFlashLight() {
        if (camera == null) {
            camera = Camera.open();
            camera.startPreview();
            Camera.Parameters parameter = camera.getParameters();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
                parameter.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            } else {
                parameter.set("flash-mode", "torch");
            }
            camera.setParameters(parameter);
            handler.removeCallbacksAndMessages(null);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    turnOffFlashLight();
                }
            }, OFF_TIME);
        }
        return true;
    }

    public boolean turnOffFlashLight() {
        if (camera != null) {
            handler.removeCallbacksAndMessages(null);
            Camera.Parameters parameter = camera.getParameters();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
                parameter.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            } else {
                parameter.set("flash-mode", "off");
            }
            camera.setParameters(parameter);
            camera.stopPreview();
            camera.release();
            camera = null;
        }
        return true;
    }

// ############################### 闪光灯 ###############################

    /**
     * 切换闪关灯，默认关闭
     */
    public static boolean toggleFlashMode(final Camera mCamera, final Camera.Parameters mParameters) {

        if (mCamera != null) {
            try {
                final String mode = mParameters.getFlashMode();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR)
                    if (TextUtils.isEmpty(mode) || Camera.Parameters.FLASH_MODE_OFF.equals(mode)) {
                        Log.e("闪关灯开启", "FLASH_MODE_TORCH");
                        setFlashMode(Camera.Parameters.FLASH_MODE_TORCH, mCamera, mParameters);
                    } else {
                        Log.e("闪关灯", "FLASH_MODE_OFF");
                        setFlashMode(Camera.Parameters.FLASH_MODE_OFF, mCamera, mParameters);
                    }
                else mParameters.set("flash-mode", "off");
                handler.removeCallbacksAndMessages(null);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setFlashMode(Camera.Parameters.FLASH_MODE_OFF, mCamera, mParameters);
                    }
                }, OFF_TIME);
                return true;
            } catch (Exception e) {
                Log.e("切换闪关灯", "toggleFlashMode", e);
            }
        }
        return false;
    }

    /**
     * 设置闪光灯
     *
     * @param value
     */
    private static boolean setFlashMode(String value, Camera camera, Camera.Parameters mParameters) {
        if (mParameters != null && camera != null) {
            try {
                if (Camera.Parameters.FLASH_MODE_TORCH.equals(value) || Camera.Parameters.FLASH_MODE_OFF.equals(value)) {
                    mParameters.setFlashMode(value);
                    camera.setParameters(mParameters);
                }
                return true;
            } catch (Exception e) {
                Log.e("设置闪光灯", "setFlashMode", e);
            }
        }
        return false;
    }
}
