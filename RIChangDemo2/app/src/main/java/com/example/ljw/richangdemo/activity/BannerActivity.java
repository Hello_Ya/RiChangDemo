package com.example.ljw.richangdemo.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.example.ljw.richangdemo.R;
import com.example.ljw.richangdemo.base.BaseActivity;
import com.example.ljw.richangdemo.utils.GlideImageLoader;
import com.orhanobut.logger.Logger;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/1/3 0003.
 * function：
 */

public class BannerActivity extends BaseActivity {
    @Bind(R.id.banner)
    Banner banner;

    @Override
    protected int setContentViewID() {
        return R.layout.activity_banner;
    }

    @Override
    protected void initView() {
        //设置属性
        setbanner();

    }

    @Override
    protected void initData() {
        //添加数据
        List<String> bannerlist = new ArrayList<>();
                bannerlist.add("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1505916308420&di=fb76a82b0772e75b10069db901b555db&imgtype=0&src=http%3A%2F%2Fimg.pconline.com.cn%2Fimages%2Fupload%2Fupc%2Ftx%2Fwallpaper%2F1306%2F14%2Fc3%2F22068471_1371197032014.jpg");
        bannerlist.add("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1505916071777&di=a41de38340fdc327dbd9372ae28d7c6d&imgtype=0&src=http%3A%2F%2Fc.hiphotos.baidu.com%2Fzhidao%2Fpic%2Fitem%2Fe61190ef76c6a7ef8cc5d559fffaaf51f2de66e9.jpg");
        bannerlist.add("http://d.ifengimg.com/mw640_q95/y1.ifengimg.com/a/2016_11/128e352eb24a41e.jpg");
        bannerlist.add("http://00.minipic.eastday.com/20170302/20170302173543_67e37db8da4801357c1816dcc8b9eeaa_1.jpeg");

        banner.setImages(bannerlist);
        banner.start();
    }


    private void setbanner() {
        // List<Integer> images = new ArrayList<>();
        //设置banner样式
        banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
        banner.setIndicatorGravity(BannerConfig.CENTER);
        //banner.setIndicatorGravity(BannerConfig.RIGHT);
        //设置图片加载器
        banner.setImageLoader(new GlideImageLoader());
        //设置图片集合
        //  banner.setImages(images);
        //设置banner动画效果
        banner.setBannerAnimation(Transformer.Default);
//        headBanner.setBannerAnimation(Transformer.CubeOut);
        //设置标题集合（当banner样式有显示title时）
//        banner.setBannerTitles("当banner样式有显示title时");
        //设置自动轮播，默认为true
        banner.isAutoPlay(false);

        //设置轮播时间
        banner.setDelayTime(2000);
        //设置指示器位置（当banner模式中有指示器时）
        banner.setIndicatorGravity(BannerConfig.CENTER);
        //banner设置方法全部调用完毕时最后调用
        banner.start();


        banner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //   Logger.e("当前点击的图片为=onPageScrolled=="+position+"===="+positionOffset+"===="+positionOffsetPixels);

            }

            @Override
            public void onPageSelected(int position) {
                Logger.e("当前点击的图片为=onPageSelected==" + position + "====");

            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });
        //点击事件
        banner.setOnBannerListener(new OnBannerListener() {

            @Override
            public void OnBannerClick(int position) {
                Logger.e("图片总数为===" );
                Logger.e("当前点击的图片为===" + position);

     /*           String url = list.get(position).getLink();
                if (StringUtil.isNullOrEmpty(url)) {
                    toast("暂无活动链接");
                } else {
                    Intent intent = new Intent(getActivity().this, WebActivity.class);
                    intent.putExtra("url", url);
                    startActivity(intent);
                }*/


            }
        });

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
