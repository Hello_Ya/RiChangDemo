package com.example.ljw.richangdemo.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ljw.richangdemo.R;
import com.example.ljw.richangdemo.adapter.LazyViewPagerAdapter;
import com.example.ljw.richangdemo.base.LazyLoadBaseFragment;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ljw on 2018/5/10.
 * function：
 */

public class LazyFragment2 extends LazyLoadBaseFragment {


    @Bind(R.id.tablayout)
    TabLayout tablayout;
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    private String text = getClass().getSimpleName() + " ";


    public static LazyFragment2 newInstance(String params) {
        LazyFragment2 fragment = new LazyFragment2();
        Bundle args = new Bundle();
        args.putString("params", params);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragmnet_home;
    }

    @Override
    protected void initView(View rootView) {

        initViewPager();
    }

    private void initViewPager() {
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new LazyFragment1());
        fragments.add(new LazyFragment2());
        fragments.add(new LazyFragment3());
        fragments.add(new LazyFragment4());

        String[] titles = {"Tab1", "Tab2", "Tab3", "Tab4"};

        viewpager.setAdapter(new LazyViewPagerAdapter(getChildFragmentManager(), fragments, titles));
        tablayout.setupWithViewPager(viewpager, false);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
