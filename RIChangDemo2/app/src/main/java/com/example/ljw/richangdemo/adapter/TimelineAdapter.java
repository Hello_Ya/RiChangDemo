package com.example.ljw.richangdemo.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ljw.richangdemo.R;
import com.example.ljw.richangdemo.bean.TimeLineBean;
import com.example.ljw.richangdemo.utils.DateTimeUtils;
import com.example.ljw.richangdemo.utils.VectorDrawableUtils;
import com.github.vipulasri.timelineview.TimelineView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.example.ljw.richangdemo.MyApplication.mContext;

/**
 * Created by ljw on 2017/12/7.
 * function：
 */

public class TimelineAdapter extends RecyclerView.Adapter<TimelineAdapter.TimeLineViewHolder> {

    private Context mcontent;
    private List<TimeLineBean> datas;
    private LayoutInflater inflater;

    public TimelineAdapter(Context context, List<TimeLineBean> timelists) {
        this.mcontent = context;
        this.datas = timelists;

    }

    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_timeline, parent, false);


        return new TimeLineViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TimeLineViewHolder holder, int position) {
        TimeLineBean timeLineBean = datas.get(position);
        if (position==0) {
                    holder.timeMarker.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_activity, R.color.colorPrimary));
        } else {
                 holder.timeMarker.setMarker((ContextCompat.getDrawable(mContext, R.drawable.ic_marker)), ContextCompat.getColor(mContext, R.color.colorPrimary));
        }


        if(!timeLineBean.getmDate().isEmpty()) {
            holder.textTimelineDate.setVisibility(View.VISIBLE);
            holder.textTimelineDate.setText(DateTimeUtils.parseDateTime(timeLineBean.getmDate(), "yyyy-MM-dd HH:mm", "hh:mm a, dd-MMM-yyyy"));
        }
        else{

            holder.textTimelineDate.setVisibility(View.GONE);
        }

        holder.textTimelineTitle.setText(timeLineBean.getmMessage());

    }

    @Override
    public int getItemCount() {
        return (datas!=null?datas.size():0);
    }


      static class TimeLineViewHolder extends RecyclerView.ViewHolder {


          @Bind(R.id.time_marker)
          TimelineView timeMarker;
          @Bind(R.id.text_timeline_date)
          TextView textTimelineDate;
          @Bind(R.id.text_timeline_title)
          TextView textTimelineTitle;

          public TimeLineViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
