package com.example.ljw.richangdemo.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ljw.richangdemo.R;
import com.example.ljw.richangdemo.base.LazyFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ljw on 2017/12/7.
 * function：
 */

public class OtherFragment extends LazyFragment {
    @Bind(R.id.tv_nae)
    TextView tvNae;

    @Override
    protected int setContentView() {
        return R.layout.fragment_other;
    }

    @Override
    protected void initView(View view) {

    }

    @Override
    protected void onUserVisible() {

    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected void onBaseDestroyView() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
