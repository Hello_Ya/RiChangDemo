package com.example.ljw.richangdemo;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.ljw.richangdemo.adapter.RecycleAdapter;
import com.example.ljw.richangdemo.base.BaseActivity;
import com.example.ljw.richangdemo.bean.DemoBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ljw on 2017/12/5.
 * function：
 */

public class RecycleviewActivity extends BaseActivity {
    @Bind(R.id.tv_status_bar)
    TextView tvStatusBar;
    @Bind(R.id.toolbar_left_iv)
    ImageView toolbarLeftIv;
    @Bind(R.id.toolbar_left_tv)
    TextView toolbarLeftTv;
    @Bind(R.id.left_view)
    RelativeLayout leftView;
    @Bind(R.id.toolbar_title)
    TextView toolbarTitle;
    @Bind(R.id.toolbar_right_iv)
    ImageView toolbarRightIv;
    @Bind(R.id.toolbar_right_tv)
    TextView toolbarRightTv;
    @Bind(R.id.right_view)
    RelativeLayout rightView;
    @Bind(R.id.toobar_view)
    RelativeLayout toobarView;
    @Bind(R.id.toolbar_main)
    LinearLayout toolbarMain;
    @Bind(R.id.toolbar)
    LinearLayout toolbar;

    @Bind(R.id.cb_all)
    CheckBox cbAll;
    @Bind(R.id.btn_ok)
    Button btnOk;
    @Bind(R.id.recyclerview)
    RecyclerView recyclerview;
    @Bind(R.id.tv_content)
    TextView tvContent;
    @Bind(R.id.ll_bottom)
    LinearLayout llBottom;
    private List<DemoBean.ListBean> mDatas;
    private RecycleAdapter adapter;

    @Override
    protected int setContentViewID() {
        return R.layout.activity_recycleview;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        mDatas = new ArrayList<>();
        for (int i = 'A'; i <= 'Z'; i++) {
            mDatas.add(new DemoBean.ListBean("" + (char) i, false));
        }

        adapter = new RecycleAdapter(this, R.layout.item_recycle, mDatas);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(linearLayoutManager);
        recyclerview.setAdapter(adapter);
        cbAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                for (int i = 0; i < mDatas.size(); i++) {
                    mDatas.get(i).setCheck(cbAll.isChecked());
                }
                if (isChecked) {
                    tvContent.setText("已选中" + mDatas.size() + "个");
                } else {
                    tvContent.setText("已选中" + a + "个");
                }

                adapter.notifyDataSetChanged();


            }
        });

    }


    public int a = 0;

    @Override
    protected void initListener() {

        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

                view.setBackgroundResource(R.color.white);

            }
        });

        adapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {

                switch (view.getId()) {
                    case R.id.cb_checked:
                        if (mDatas.get(position).isCheck()) {
                            a++;
                        } else {
                            if (a == 0) {

                            } else {
                                a--;
                            }
                        }
                        tvContent.setText("已选中" + a + "个");
                        if (mDatas.size() == a) {
                            cbAll.setChecked(true);
                        }
                        break;

                }


            }
        });


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.toolbar_left_iv, R.id.btn_ok})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.toolbar_left_iv:

                break;
            case R.id.btn_ok:
                String str = "";
                int a = 0;
                for (int i = 0; i < mDatas.size(); i++) {
                    if (mDatas.get(i).isCheck()) {
                        str += mDatas.get(i).getmTitle() + "-";
                        ++a;
                    }
                }
                if (!str.isEmpty())
                    toast(str.substring(0, str.length() - 1) + "\n选中了" + a + "个");
                break;
        }
    }
}
