package com.example.ljw.richangdemo.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ljw.richangdemo.R;
import com.example.ljw.richangdemo.adapter.TimelineAdapter;
import com.example.ljw.richangdemo.base.LazyFragment;
import com.example.ljw.richangdemo.bean.TimeLineBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.example.ljw.richangdemo.R.id.recyclerview;

/**
 * Created by ljw on 2017/12/7.
 * function：
 */

public class TimeLineFragment extends LazyFragment {

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
        private List<TimeLineBean> datas=new ArrayList<>();
    @Override
    protected int setContentView() {
        return R.layout.fragment_timeline;
    }

    @Override
    protected void initView(View view) {
            getDatas();

        TimelineAdapter adapter=new TimelineAdapter(getActivity(),datas);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

    }

    private void getDatas() {
            datas.add(new TimeLineBean("已查过发撒法","2017-01-12 08:00",true));
            datas.add(new TimeLineBean("发撒回家","2017-02-12 08:00",false));
            datas.add(new TimeLineBean("ifds膜法世家","2017-03-12 08:00",false));
            datas.add(new TimeLineBean("你发的范德萨","2017-05-12 08:00",false));
            datas.add(new TimeLineBean("全额返对方的地方","2017-06-12 08:00",false));
            datas.add(new TimeLineBean("发到付款了方式","2017-07-12 08:00",false));
            datas.add(new TimeLineBean("哦啊都免费流量的","2017-09-12 08:00",false));

    }

    @Override
    protected void onUserVisible() {

    }

    @Override
    protected void onUserInvisible() {

    }

    @Override
    protected void onBaseDestroyView() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
